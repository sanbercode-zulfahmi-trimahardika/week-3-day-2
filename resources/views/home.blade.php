<!DOCTYPE html>
<html>
  <head>
    <title> SanberBook </title>
    <meta charset="UTF-8">
  </head>

  <body>

    <!-- Ini Bagian Headernya -->
    <div>
        <h1> SanberBook </h1>
        <h2> Social Media Developer Santai Berkualitas </h2>
        <p>
            Belajar dan Berbagi agar hidup ini semakin santai berkualitas
        </p>
    </div>

    <!-- Ini Bagian Benefit -->
    <div>
        <h3> Benefit Join di SanberBook </h3>
        <ul>
            <li> Mendapatkan motivasi dari sesama developer </li>
            <li> Sharing knowledge dari para mastah Sanber </li>
            <li> Dibuat oleh calon web developer terbaik </li>
        </ul>
    </div>

    <!-- Ini Bagian Cara Bergabung -->
    <div>
        <h3> Cara Bergabung ke SanberBook</h3>
        <ol>
            <li> Mengunjungi Website ini </li>
            <li> Mendaftar di <a href="<?php echo url('/register');?>" target="_blank"> Form Sign Up </a></li>
            <li> Selesai! </li>
        </ol>
    </div>

  </body>
</html>
