<!DOCTYPE html>
<html>
  <head>
    <title> Form Sign Up SanberBook </title>
    <meta charset="UTF-8">
  </head>

  <body>

    <!-- Ini Bagian Header -->
    <h1> Buat Account Baru! </h1>
    <h3> Sign Up Form </h3>

    <!-- Ini Bagian Form -->
    <form method="post" onsubmit="confirmInput()" action="/welcome" name="sign_up">
        @csrf
        <!-- Ini Bagian Form Nama -->
        <label for="first_name"> First name: </label> <br><br>
        <input type="text" name="first_name" id="first_name" required> <br><br>
        <label for="last_name"> Last Name: </label> <br><br>
        <input type="text" name="last_name" id="first_name" required> <br><br>

        <!-- Ini Bagian Form Gender -->
        <label>Gender:</label> <br><br>
        <input type="radio" name="gender" value="male" required>Male <br>
        <input type="radio" name="gender" value="female" required>Female <br>
        <input type="radio" name="gender" value="other" required>Other <br><br>

        <!-- Ini Bagian Nationality -->
        <label>Nationality</label> <br><br>
        <select>
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select> <br><br>

        <!-- Ini Bagian Language Spoken -->
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" name="language_spoken" value="bahasa_indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" name="language_spoken" value="english"> English <br>
        <input type="checkbox" name="language_spoken" value="other"> Other <br><br>

        <!-- Ini Bagian Bio -->
        <label>Bio:</label> <br><br>
        <textarea cols="40" rows="10"></textarea> <br>

        <!-- Ini Button -->
        <input type="submit" value="Sign Up">
    </form>

  </body>
</html>
